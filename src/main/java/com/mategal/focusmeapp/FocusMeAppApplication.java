package com.mategal.focusmeapp;

import com.mategal.focusmeapp.view.MainView;
import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FocusMeAppApplication extends AbstractJavaFxApplicationSupport {




    public static void main(String[] args){

        launch(FocusMeAppApplication.class, MainView.class, args);
    }
}
