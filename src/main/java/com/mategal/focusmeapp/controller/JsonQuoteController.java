package com.mategal.focusmeapp.controller;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mategal.focusmeapp.model.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;

@Component
public class JsonQuoteController {

    String _autor = new String();
    String _quote = new String();
    @Autowired
    private Quote quote;

    public void getJsonFromURL(String url) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            URL jsonUrl = new URL(url);

            JsonNode jsonNode = objectMapper.readTree(jsonUrl);

            _autor = jsonNode.get(0).get("title").toString();
            _quote = jsonNode.get(0).get("content").toString();

            stringFormatter();

            quote.setAutor(_autor);
            quote.setQuote(_quote);
        } catch (IOException e) {
            quote.setQuote("Internet connection failed...");
            quote.setAutor("");
        }


    }

    private void stringFormatter() {
        _quote = _quote.replace("<p>", "").replace("</p>", "").replace("\\n", "");
        _autor = _autor.replace("\"", "");
    }
}
