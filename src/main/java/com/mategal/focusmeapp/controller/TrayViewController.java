package com.mategal.focusmeapp.controller;

import com.mategal.focusmeapp.model.AppActivityTracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
@Component
public class TrayViewController {
    @Autowired
    private AppActivityTracker activityTracker;

    public TrayViewController() {
        /* Use an appropriate Look and Feel */
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            //UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        /* Turn off metal's use of bold fonts */
        UIManager.put("swing.boldMetal", Boolean.FALSE);
        //Schedule a job for the event-dispatching thread:
        //adding TrayIcon.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    //Obtain the image URL
    protected static Image createImage(String path, String description) {
        URL imageURL = TrayViewController.class.getResource(path);

        if (imageURL == null) {
            System.err.println("Resource not found: " + path);
            return null;
        } else {
            return (new ImageIcon(imageURL, description)).getImage();
        }
    }

    private void createAndShowGUI() {
        //Check the SystemTray support
        if (!SystemTray.isSupported()) {
            System.out.println("SystemTray is not supported");
            return;
        }
        final PopupMenu popup = new PopupMenu();
        Image image = Toolkit.getDefaultToolkit().getImage("C:\\Users\\mateu\\OneDrive\\MyApps\\FocusMeApp\\src\\main\\resources\\images\\focus-icon.png");
        final TrayIcon trayIcon = new TrayIcon(image, "FocusME");
        trayIcon.setImageAutoSize(true);
        final SystemTray tray = SystemTray.getSystemTray();

        // Create a popup menu components
        MenuItem activationItem = new MenuItem(activityTracker.getStatusName());
        MenuItem pomodoroItem = new MenuItem("Pomodoro");
        MenuItem toDoListItem = new MenuItem("ToDoList");
        MenuItem exitItem = new MenuItem("Exit");

        toDoListItem.setEnabled(false);

        //Add components to popup menu
        popup.add(activationItem);
        popup.addSeparator();
        popup.add(pomodoroItem);
        popup.addSeparator();
        popup.add(toDoListItem);
        popup.addSeparator();
        popup.add(exitItem);

        trayIcon.setPopupMenu(popup);



        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.out.println("TrayIcon could not be added.");
            return;
        }

        trayIcon.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            }
        });

        activationItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                activationItem.setLabel(activityTracker.getStatusName());
                activityTracker.switchActivityState();
            }
        });

        pomodoroItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

//       TODO: 05.10.2018 - that feature will be in the future
//        toDoListItem.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//
//            }
//        });

        exitItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tray.remove(trayIcon);
                System.exit(0);
            }
        });
    }
}
