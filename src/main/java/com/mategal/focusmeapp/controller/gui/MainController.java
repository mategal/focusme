package com.mategal.focusmeapp.controller.gui;

import com.mategal.focusmeapp.controller.JsonQuoteController;
import com.mategal.focusmeapp.model.AppActivityTracker;
import com.mategal.focusmeapp.model.Quote;
import de.felixroske.jfxsupport.FXMLController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URL;
import java.util.ResourceBundle;


@FXMLController
public class MainController {
    @Autowired
    private AppActivityTracker activityTracker;

    @Autowired
    private JsonQuoteController jsonQuoteController;

    @Autowired
    private Quote quote;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button hideButton;

    @FXML
    private Text quoteText;

    @FXML
    void hideButton(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void initialize() {
        activityTracker.setActivated(false);

        jsonQuoteController.getJsonFromURL("http://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1");
        quoteText.setText(quote.getQuote() + " - " + quote.getAutor());
    }


}