package com.mategal.focusmeapp.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Observable;


@Component
@Data
public class AppActivityTracker extends Observable {

    private boolean activated;

    public AppActivityTracker(boolean activated) {
        this.activated = activated;
    }

    public void switchActivityState(){
        activated = !activated;
        setChanged();
        notifyObservers();
    }

    public String getStatusName(){
        if(activated){
            return "Activated";
        }else{
            return "Deactivated";
        }
    }


}
