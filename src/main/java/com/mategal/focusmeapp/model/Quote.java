package com.mategal.focusmeapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.stereotype.Component;


@Data
@Component
public class Quote {
    @JsonProperty("content")
    private String quote;
    @JsonProperty("title")
    private String autor;

    public Quote() {
    }
}
