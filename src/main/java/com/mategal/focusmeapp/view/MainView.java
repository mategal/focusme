package com.mategal.focusmeapp.view;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

@FXMLView("/fxml/Main.fxml")
public class MainView extends AbstractFxmlView {
}
